#include <iostream>
using namespace std;

void Print(int Matrix[5][5])
{
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			cout << Matrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

int main()
{
	time_t t = time(0);
	int Matrix[5][5] = { 0 };
	int Day = (localtime(&t)->tm_mday);
	int Sum = 0;

	cout << "Starter matrix is:" << endl;
	Print(Matrix);

	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			Matrix[i][j] = i + j;
		}
	}

	cout << "New matrix is:" << endl;
	Print(Matrix);

	cout << "Day of mounth is: "<< Day;
	cout << endl;

	for (int j = 0; j < 5; j++)
	{
		Sum = Sum + Matrix[Day % 5][j];
	}
	cout << "Summa is: " << Sum;

	return 0;
}
